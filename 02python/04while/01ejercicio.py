#Ejercicio 1
x = 1
while x < 11:
    if x % 2 == 1:
        print (f"Es impar {x}")
    elif x % 2 == 0:
        print (f"Es par {x}")
    x += 1
    
    

#Ejercicio 2
n = 3

while n > 0:
    print(n + 1)
    n -= 1
else:
    print(n)