from ctypes.wintypes import HACCEL


A = 5
B = 2
C = 5
D = 2
E = "Perro"
F = "perro"
G = True
H = 10
I = 10.0
print (A == B)
print (A == C)
print ((True) == (True))
print (E == F)
print (H == I)

# not True or not False = True
# not False and not True and not True
# not o >= 9 pel valor o = 5
# w == 2 and 9 >= w pel valor w = 4
# Comparar dues cadenas "Perro" i "perro" i indicar si son iguals