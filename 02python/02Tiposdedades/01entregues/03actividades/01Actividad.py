#Indiaca perquè són valides o no le següents variables
from unicodedata import name

#Puede ser minuscula o mayuscula
a = 3
#No es ni mayuscula ni letra
3 = 3
#Empiza por minuscula
a3 = 4
#No empieza ni por mayuscula ni minuscula
3a = 14
#Todo correcto tiene minusculas y guion bajo
a_b_c__95 = 1414
#Keyword
another-name = 123
#Tiene guion bajo
_abc = 131
# Es un keyword
with = 13
#Empieza por guion bajo
_3a = 13
# Empieza por numero
3_a = 12

a = "Hola"
b = "Francesc"
print (a + b,end="")